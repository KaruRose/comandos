#PostgreSql

## Crear una base de datos de postgresql:

Antes de crear la base de datos se debe crear las credenciales de acceso del
usuario que la gestionará, por lo que es necesario ejecutar el comando:

    $ sudo -u postgres psql

Esto dará acceso a la consola de PostgreSQL, una vez allí se debe ejecutar el comando:

    CREATE USER recargas_today WITH ENCRYPTED PASSWORD 12341234 CREATEDB;

La instrucción anterior creará el usuario con el cual interactuar con la base de datos.

Posteriormente se debe ejecutar la siguiente instrucción para crear la base de datos:

    CREATE DATABASE recargas_today WITH OWNER recargas_today;

Para poder crear los scripts de PostGIS se debe agregar al usuario recién creado
al grupo de super usuario de postgres para lo cual se debe ejecutar la
instrucción:

    ALTER USER recargas_today WITH SUPERUSER;

Una vez ejecutadas las instrucciones con el usuario postgres se debe salir de la
consola ejecutando el comando:

    \q

y luego se debe salir del usuario postgres con la instrucción

    exit

Ahora se debe ejecutar la consola con el usuario recién creado, para lo cual se
ejecuta la instrucción:

    psql -U recargas_today -h servidor_de_base_de_datos -d recargas_today



##Establecer nueva secuencia de PK id
-- Get Max ID from table
`SELECT MAX(id) FROM table;`

-- Get Next ID from table
`SELECT nextval('table_id_seq');`

-- Set Next ID Value to MAX ID
`SELECT setval('table_id_seq', (SELECT MAX(id) FROM table)+1);`